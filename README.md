# STAY VIBRANT

⭐ 保持活力 ⭐

## 🤔 这是什么

这是一个小网站，你可以在这上面收听我挑选的电子音乐。所有视频都来自 YouTube。

[STAY VIBRANT ↑%](https://stay-vibrant.soraharu.com/)

🎛️🎚️🎧🎙️🎹🥁

## ⚙️ 部署至 Vercel

1. 克隆本仓库到 [GitHub.com](https://github.com/) 或 [GitLab.com](https://gitlab.com/) 个人仓库
2. 在 [Vercel](https://vercel.com/) 新建项目到你的个人仓库
3. 在 Vercel 内填写项目 Environment Variables，Name 为 `REACT_APP_VIDEO_SOURCE`，Value 为 `youtube`
4. 等待自动部署完成
5. 编辑 `src/playlists.ts` 以添加曲目

## 📜 开源许可

基于 [WTFPL](https://choosealicense.com/licenses/wtfpl/) 许可进行开源。
